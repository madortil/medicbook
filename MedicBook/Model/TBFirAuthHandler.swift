//
//  TBFirAuthHandler.swift
//  TilBook
//
//  Created by Dan Fechtmann on 08/12/2018.
//  Copyright © 2018 Dan Fechtmann. All rights reserved.
//

import Foundation
import FirebaseAuth

class TBFirAuthHandler {
    
    static let instance = TBFirAuthHandler()
    private init(){}
    
    func loginAnonym(onComplete:@escaping ()->(), onFail:@escaping (_ e: Error)->()) {
        Auth.auth().signInAnonymously { (user, error) in
            if let err = error {
               onFail(err)
            }
            else {
               onComplete()
            }
        }
    }
    
}

