//
//  FirDatabaseHandler.swift
//  TilBook
//
//  Created by Dan Fechtmann on 19/03/2018.
//  Copyright © 2018 Dan Fechtmann. All rights reserved.
//

import Foundation
import Firebase
import FirebaseAuth

//TODO fix loading activity not having activity instance
final class TBFirDatabaseHandler {
    
    let FILE_EXTENSION = ".pdf"
    let USERS_NODE = "users"
    
    static let instance = TBFirDatabaseHandler()
    private init() {}
    
    func deleteDatabaseCategory(categoryKey: String, onComplete:@escaping ()->Void, onFail:@escaping ()->()) {
        let dbRef = Database.database().reference().child(.BOOK_GROUP).child(categoryKey)
            dbRef.observeSingleEvent(of: .value)
        { (snap) in
                let children = snap.childSnapshot(forPath: .PAGE_GROUP).hasChildren()
            if (children) {
                onFail()
            }
            else {
                dbRef.removeValue(completionBlock: { (error, ref) in
                    onComplete()
                })
            }
        }
    }
    
    func readDatabaseToMemory(onComplete:@escaping ()->()) {
        Database.database().reference().child(.BOOK_GROUP).observe(.value) { (dataSnap) in
            categories.removeAll()
            for category in dataSnap.children.allObjects as! [DataSnapshot] {
                let name = category.childSnapshot(forPath: .NAME).value as! String
                let pages = self.readPagesFromSnap(datasnap: category.childSnapshot(forPath: .PAGE_GROUP).children.allObjects as! [DataSnapshot])
                let key = category.key
                
                var newCategory = TBCategory(name: name, categoryKey: key)
                newCategory.pages = pages
                categories.append(newCategory)
            }
            
            categories.sort { $0.name < $1.name }
            for index in categories.indices {
                categories[index].pages.sort { $0.name < $1.name }
            }
            Database.database().reference().removeAllObservers()
            onComplete()
        }
    }
    
    func setNewPageInDatabase(categoryKey: String, onPageAdded:@escaping () -> Void, onPageFailedToAdd:@escaping (_ e: Error)->()) {
        let newPageRef = Database.database().reference().child(.BOOK_GROUP).child(categoryKey).child(.PAGE_GROUP).childByAutoId()
        let newPageURL = newPageRef.key! + FILE_EXTENSION
        newPageRef.updateChildValues([String.NAME: .DEFAULT_PAGE_NAME, String.URL_CODE: newPageURL]) { (error, databaseRef) in
            if let err = error {
                print(err.localizedDescription)
                onPageFailedToAdd(err)
            }
            else {
                TBFirStorageHandler.instance.uploadSamplePDF(pdfURL: newPageURL, onComplete: {
                    onPageAdded()
                }, onFail: { (e) in
                    onPageFailedToAdd(e)
                })
            }
        }
    }
    
    func setNewCategoryInDatabase(onComplete:@escaping () -> Void, onFail:@escaping (_ e: Error)->()) {
        let categoryReference = Database.database().reference().child(.BOOK_GROUP).childByAutoId()
        categoryReference.updateChildValues([String.NAME: String.DEFAULT_CATEGORY_NAME]) { (error, dbRef) in
            self.setNewPageInDatabase(categoryKey: categoryReference.key!, onPageAdded: onComplete, onPageFailedToAdd: onFail)
        }
    }
    
    func setNewCategoryName(categoryKey: String, newName: String, onComplete:@escaping () -> (), onFail:@escaping (_ e: Error)->()) {
        Database.database().reference().child(.BOOK_GROUP).child(categoryKey).updateChildValues([String.NAME: newName]) { (error, dbRef) in
            if let err = error {
                onFail(err)
            }
            else {
                onComplete()
            }
        }
    }
    
    func setNewPageName(categoryKey: String, pageKey: String, newName: String, onComplete:@escaping () -> Void) { Database.database().reference().child(.BOOK_GROUP).child(categoryKey).child(.PAGE_GROUP).child(pageKey).updateChildValues([String.NAME: newName]) { (error, dbRef) in
            if let err = error {
                //TODO implement handling if error
                print(err.localizedDescription)
            }
            else {
                onComplete()
            }
        }
    }
    
    func deletePage(categoryKey: String, pageKey: String, onComplete:@escaping () -> Void, onFail:@ escaping (_ e:Error) -> ()) {
        Database.database().reference().child(.BOOK_GROUP).child(categoryKey).child(.PAGE_GROUP).child(pageKey).removeValue { (error, dbRef) in
            guard let e = error
            else {
                onComplete()
                return
            }
            onFail(e)
        }
    }
    
    func observePageChange() {
    Database.database().reference().child(.BOOK_GROUP).child(currentCategory.categoryKey).child(.PAGE_GROUP).observe(.value) { (snap) in
        print("hello")
        currentCategory.pages.removeAll()
        currentCategory.pages = self.readPagesFromSnap(datasnap: snap.children.allObjects as! [DataSnapshot])
        }
    }

    func createAlert(title: String, text: String) -> UIAlertController {
        let alert = UIAlertController(title: title, message: text, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "אוקיי", style: .default, handler: nil)
        alert.addAction(okAction)
        return alert
    }
    
}

extension TBFirDatabaseHandler {
    
    
    private func readPagesFromSnap(datasnap: [DataSnapshot]) -> [TBPage] {
        var pagesToReturn: [TBPage] = []
        
        for pageSnap in datasnap {
            let name = pageSnap.childSnapshot(forPath: String.NAME).value as! String
            let url = pageSnap.childSnapshot(forPath: String.URL_CODE).value as! String
            let key = pageSnap.key
            pagesToReturn.append(TBPage(name: name, url: url, pageKey: key))
        }
        pagesToReturn.sort { $0.name < $1.name }
        return pagesToReturn
    }
}


enum FirDatabaseHandlerError: Error {
    case valueCannotBeNull
}
