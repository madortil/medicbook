//
//  Extensions.swift
//  MedicBook
//
//  Created by Mador Til on 14/01/2019.
//  Copyright © 2019 Yahav Levi. All rights reserved.
//

import Foundation

extension Date {
    static var hourPeriod: HourPeriod {
        let morningHour = 7
        let noonHour = 15
        let nightHour = 21
        switch Calendar.current.component(.hour, from: Date()) {
        case let x where noonHour>x && x>=morningHour:
            return .morning
        case let x where nightHour>x && x>=noonHour:
            return .noon
        default:
            return .night
        }
    }
    
    enum HourPeriod {
        case morning
        case noon
        case night
    }
}
