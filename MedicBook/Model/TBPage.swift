//
//  Page.swift
//  TilBook
//
//  Created by Dan Fechtmann on 19/03/2018.
//  Copyright © 2018 Dan Fechtmann. All rights reserved.
//

import Foundation

/*
 The Page class is used to store the data of each Categories' page's data which consists of a displayed page name and hidden page url
 */

struct TBPage {
    var name: String = ""
    var url : String = ""
    var pageKey: String = ""
    init(name: String, url:String, pageKey: String) {
        self.name = name
        self.url = url
        self.pageKey = pageKey
    }
}
