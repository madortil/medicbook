//
//  DataHolder.swift
//  TilBook
//
//  Created by Dan Fechtmann on 19/03/2018.
//  Copyright © 2018 Dan Fechtmann. All rights reserved.
//

import Foundation


//final variables to call database

 let MAX_DOWNLOAD_SIZE : Int64 = Int64(99999999999999)

 var isAdmin = false

 var currentPage: TBPage = TBPage(name: "", url: "", pageKey: "")
 var currentCategory: TBCategory = TBCategory(name: "", categoryKey: "")

 var existingData: [String] = []
 var categories : [TBCategory] = []

 let cache = NSCache<NSString, NSData>()

extension String {
    static let BOOK_GROUP = "books"
    static let NAME = "name"
    static let PAGE_GROUP = "pages"
    static let URL_CODE = "url"
    static let PASS_CODE = "passcode"
    static let SAMPLE_PDF = "nana.pdf"
    static let DEFAULT_PAGE_NAME = "עמוד חדש"
    static let DEFAULT_CATEGORY_NAME = "קטגוריה חדשה"
    static let FIRST_RUN = "firstRun"
    static let DELETE_TITLE = "מחק"
    static let EDIT_TITLE = "ערוך"
}
