//
//  Category.swift
//  TilBook
//
//  Created by Dan Fechtmann on 19/03/2018.
//  Copyright © 2018 Dan Fechtmann. All rights reserved.
//

import Foundation

/*
 The Category class contains the data of all the categories in the database including their pages' data
 */

struct TBCategory {
    
    var name : String = ""
    var categoryKey: String = ""
    var pages : [TBPage] = []
    
    init(name: String, categoryKey: String) {
        self.name = name
        self.categoryKey = categoryKey
    }
}
