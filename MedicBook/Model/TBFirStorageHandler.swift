//
//  FirStorageHandler.swift
//  TilBook
//
//  Created by Dan Fechtmann on 25/03/2018.
//  Copyright © 2018 Dan Fechtmann. All rights reserved.
//
import Foundation
import Firebase

final class TBFirStorageHandler {
    
    static let instance = TBFirStorageHandler()
    private init() {}
    
    func uploadSamplePDF(pdfURL: String, onComplete:@escaping () -> Void, onFail:@escaping (_ e: Error)->()) {
        //TODO implement activating ezload
        Storage.storage().reference().child(.SAMPLE_PDF).getData(maxSize: Int64(MAX_DOWNLOAD_SIZE)) { (data, error) in
            if let err = error {
                onFail(err)
                print(err.localizedDescription)
            }
            else {
                self.upload(pdfURL: pdfURL, data: data!, oncComplete: onComplete, onFail: onFail)
            }
        }
    }
    
    func upload(pdfURL: String, data: Data, oncComplete:@escaping () -> Void, onFail:@escaping (_ e: Error) -> Void) {
        let metaData = StorageMetadata()
        metaData.contentType = "application/pdf"
        Storage.storage().reference().child(pdfURL).putData(data, metadata: metaData) { (storageMeta, error) in
            if let err = error {
                print(err.localizedDescription)
                onFail(err)
            }
            else {
                oncComplete()
            }
        }
    }
    
    func deletePDFFileFromDatabase(categoryKey: String, pageKey: String, pdfURL: String, onComplete:@escaping () -> Void, onFail:@escaping (_ e: Error) -> ()) {
        Storage.storage().reference().child(pdfURL).delete { (error) in
            TBFirDatabaseHandler.instance.deletePage(categoryKey: categoryKey, pageKey: pageKey, onComplete: onComplete, onFail: onFail)
        }
    }
    
}
