//
//  loginEmailAndPasswordViewController.swift
//  TilBook
//
//  Created by Dan Fechtmann on 28/08/2018.
//  Copyright © 2018 Dan Fechtmann. All rights reserved.
//

import UIKit
import Firebase

class TBLoginEmailAndPasswordViewController: UIViewController {
    
    let INPUT_EMAIL_PASS_ERROR_MSG = "יש למלא דואר אלקטרוני וסיסמא."
    let CATEGORIES_SEAGUE_IDENTIFIER = "showCategories"
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var backgroundImage: UIImageView!
    @IBOutlet weak var loginButton: UIButton!
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        switch Date.hourPeriod {
        case .morning:
            backgroundImage.image = #imageLiteral(resourceName: "Screen Background - MorningR")
            loginButton.setTitleColor(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), for: .normal)
        case .noon:
            backgroundImage.image = #imageLiteral(resourceName: "Screen Background - NoonR")
            loginButton.setTitleColor(#colorLiteral(red: 0.9019607843, green: 0.5333333333, blue: 0.2745098039, alpha: 1), for: .normal)
        default:
            backgroundImage.image = #imageLiteral(resourceName: "Screen Background - NightR")
            loginButton.setTitleColor(#colorLiteral(red: 0.8392156863, green: 0.8392156863, blue: 0.8431372549, alpha: 1), for: .normal)
        }
    }
    @IBAction func loginDidTouch(_ sender: Any) {
        if isTextFieldNil(textField: emailTextField) || isTextFieldNil(textField: passwordTextField) {
            let alert = AlertCreator.createErrorAlert(errorText: INPUT_EMAIL_PASS_ERROR_MSG)
            present(alert, animated: true, completion: nil)
        }
        else {
            let loading = LoadingIndicatorController.openLoadingIndicator(controller: self)
            present(loading, animated: true) {
                Auth.auth().signIn(withEmail: self.emailTextField.text!, password: self.passwordTextField.text!) { (result, e) in
                    DispatchQueue.main.async {
                        self.loginCompletionHandler(callBack: result, error: e, loading: loading)
                    }
                }
            }
        }
    }
    
    private func isTextFieldNil(textField: UITextField) -> Bool {
        if textField.text == nil{
            return true
        }
        return false
    }

}

extension TBLoginEmailAndPasswordViewController {
    func loginCompletionHandler(callBack: AuthDataResult?, error: Error?, loading: UIViewController) {
        if let e = error {
            loading.dismiss(animated: true) {
                let alert = AlertCreator.createErrorAlert(errorText: e.localizedDescription)
                self.present(alert, animated: true, completion: nil)
            }
        }
        else {
            let me = self
            isAdmin = true
            TBFirDatabaseHandler.instance.readDatabaseToMemory {
                DispatchQueue.main.async {
                    loading.dismiss(animated: true, completion: {
                        me.performSegue(withIdentifier: me.CATEGORIES_SEAGUE_IDENTIFIER, sender: me)
                    })
                }
            }
        }
    }
}
