//
//  CategoryDisplayViewController.swift
//  TilBook
//
//  Created by Dan Fechtmann on 19/03/2018.
//  Copyright © 2018 Dan Fechtmann. All rights reserved.
//

import UIKit

/*
 This class controls the displaying of all avalible categories in a single
 orginized table view, it gets the classes from the 
 
 In addition is handles the onclick of each category and transference to
 the next activity which contains all the relevant pages
 
 and finally it handles the editing/addition and deletion of categories
 
 NOTE: it's impossible to delete a category without first deleting all associated pages, it's the admin's responsibility to do this
 
 */

class TBCategoryDisplayViewController: UIViewController, UITableViewDataSource {

    let DONE_EDITING_TITLE = "סיים לערוך"
    let CATEGORY_CELL_IDENTIFIER = "categoryCell"
    let PAGE_DISPLAY_SEGUE_NAME = "showPageDisplayViewController"

    var previousViewController: UIViewController?
    
    @IBOutlet var catergoryTableView: UITableView!
    @IBOutlet var addCategoryButton: UIBarButtonItem!
    @IBOutlet weak var backgroundImage: UIImageView!
    
    //UI
    func createDeleteAler(categoryIndex: Int) -> UIAlertController {
        let me = self
        let alert = UIAlertController(title: "מחיקה", message: "אתה בטוח שברצונך למחוק את הקובץ?", preferredStyle: .alert)
        let acceptAction = UIAlertAction(title: "מחק", style: .destructive) { (action) in
            let loading = LoadingIndicatorController.openLoadingIndicator(controller: self)
            me.present(loading, animated: true, completion: {
                TBFirDatabaseHandler.instance.deleteDatabaseCategory(categoryKey: categories[categoryIndex].categoryKey, onComplete: {
                    DispatchQueue.main.async {
                        loading.dismiss(animated: true, completion: {
                            me.catergoryTableView.reloadData()
                        })
                    }
                }, onFail: {
                    DispatchQueue.main.async {
                        loading.dismiss(animated: true, completion: {
                            let alert = AlertCreator.createErrorAlert(errorText: "עליך למחוק את כל העמודים לפני מחיקת הקטגוריה!")
                            me.present(alert, animated: true, completion: nil)
                        })
                    }
                })
            })
        }
        let cancelAction = UIAlertAction(title: "בטל", style: .default, handler: nil)
        alert.addAction(acceptAction)
        alert.addAction(cancelAction)
        return alert
    }
    
    func createEditAlert(currentCategoryIndex : Int) -> UIAlertController {
        let currentCategory = categories[currentCategoryIndex]
        let alert = UIAlertController(title: "חלון עריכה", message: currentCategory.name, preferredStyle: .alert)
        alert.addTextField { (textField) in
            textField.placeholder = "שם חדש"
        }
        let alertAccept = UIAlertAction(title: "שנה", style: .destructive) { (action) in
            if let newName = alert.textFields![0].text {
                TBFirDatabaseHandler.instance.setNewCategoryName(categoryKey: categories[currentCategoryIndex].categoryKey, newName: newName, onComplete: {
                    DispatchQueue.main.async {
                        self.catergoryTableView.reloadData()
                    }
                }, onFail: {e in
                    let alert = AlertCreator.createErrorAlert(errorText: e.localizedDescription, onOkay: nil)
                    self.present(alert, animated: true, completion: nil)
                })
            }
            else {
                let alert = AlertCreator.createErrorAlert(errorText: "אנא הכניסו שם חדש", onOkay: nil)
                self.present(alert, animated: true, completion: nil)
            }
        }
        let alertCancel = UIAlertAction(title: "בטל", style: .cancel, handler: nil)
        
        alert.addAction(alertAccept)
        alert.addAction(alertCancel)
        
        return alert
    }

}

//Overrides
extension TBCategoryDisplayViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        previousViewController?.dismiss(animated: false, completion: nil)
        
        catergoryTableView.delegate = self
        catergoryTableView.dataSource = self
        
        addCategoryButton.isEnabled = isAdmin
        
        if(traitCollection.forceTouchCapability == .available) {
            registerForPreviewing(with: self, sourceView: view)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        switch Date.hourPeriod {
        case .morning:
            backgroundImage.image = #imageLiteral(resourceName: "Screen Background - MorningR")
        case .noon:
            backgroundImage.image = #imageLiteral(resourceName: "Screen Background - NoonR")
        default:
            backgroundImage.image = #imageLiteral(resourceName: "Screen Background - NightR")
        }
    }
    
    override open var shouldAutorotate: Bool {
        return true
    }
    
    override open var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    override open var preferredInterfaceOrientationForPresentation: UIInterfaceOrientation {
        return .portrait
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        cache.removeAllObjects()
    }
}

//IBActions
extension TBCategoryDisplayViewController {
    
    @IBAction func addNewCategoryDidTouch(_ sender: Any) {
        let me = self
        let loading = LoadingIndicatorController.openLoadingIndicator(controller: self)
        present(loading, animated: true) {
            TBFirDatabaseHandler.instance.setNewCategoryInDatabase(onComplete: {
                DispatchQueue.main.async {
                    loading.dismiss(animated: true, completion: {
                        me.catergoryTableView.reloadData()
                    })
                }
            }, onFail: { (e) in
                DispatchQueue.main.async {
                    loading.dismiss(animated: true, completion: {
                        let alert = AlertCreator.createErrorAlert(errorText: e.localizedDescription)
                        me.present(alert, animated: true, completion: nil)
                    })
                }
            })
        }
    }
    
    
}

//Tableview delegate
extension TBCategoryDisplayViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categories.count
    }
    
    //TODO add hiding of editing button if not admin
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CATEGORY_CELL_IDENTIFIER) as! TBCategoryDisplayTableViewCell
        cell.categoryNameLabel.text = categories[indexPath.row].name
        switch Date.hourPeriod {
        case .morning:
            cell.categoryNameLabel.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        case .noon:
            cell.categoryNameLabel.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        default:
            cell.categoryNameLabel.textColor = #colorLiteral(red: 0.8392156863, green: 0.8392156863, blue: 0.8431372549, alpha: 1)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let editAction = UITableViewRowAction(style: .normal, title: .EDIT_TITLE) { (action, indexPath) in
            self.present(self.createEditAlert(currentCategoryIndex: indexPath.row), animated: true, completion: nil)
        }
        
        let deleteAction = UITableViewRowAction(style: .destructive, title: .DELETE_TITLE) { (action, indexPath) in
            //TODO add check if the pages are null, then delete
            let alert = self.createDeleteAler(categoryIndex: indexPath.row)
            self.present(alert, animated: true, completion: nil)
        }
        return [editAction, deleteAction]
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        currentCategory = categories[indexPath.row]
        tableView.deselectRow(at: indexPath, animated: true)
        performSegue(withIdentifier:PAGE_DISPLAY_SEGUE_NAME, sender: self)
    }
    
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return isAdmin
    }
}

//Preview delegate
extension TBCategoryDisplayViewController: UIViewControllerPreviewingDelegate {
    
    func previewingContext(_ previewingContext: UIViewControllerPreviewing, viewControllerForLocation location: CGPoint) -> UIViewController? {
        guard let indexPath = catergoryTableView.indexPathForRow(at: location) else {return nil}
        guard let cell = catergoryTableView.cellForRow(at: indexPath) as? TBCategoryDisplayTableViewCell else {return nil}
        guard let previewVC = storyboard?.instantiateViewController(withIdentifier: "pagesView") as? TBPageDiplayViewController else {return nil}
        previewVC.preferredContentSize = CGSize(width: 300, height: 300)
        previewingContext.sourceRect = cell.frame
        return previewVC
    }
    
    func previewingContext(_ previewingContext: UIViewControllerPreviewing, commit viewControllerToCommit: UIViewController) {
        showDetailViewController(viewControllerToCommit, sender: self)
    }
}
