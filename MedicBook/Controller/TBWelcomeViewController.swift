//
//  ViewController.swift
//  TilBook
//
//  Created by Dan Fechtmann on 19/03/2018.
//  Copyright © 2018 Dan Fechtmann. All rights reserved.
//

import UIKit
import Firebase

/*
 Main openning screen, it checks for the newest books after signing you in annonimously,
 after which is promptly asks you to either continue as a user or enter a password in another screen
 to become and admin, which can edit categories, pages and the pdf files
 
 All the data is read from FirebaseDatabase and stored in a special static container called Dataholder
 */
class TBWelcomeViewController: UIViewController {
    
    let SEGUE_USER_TO_CATS = "showCategoriesFromMain"
    let WELCOME_SCREEN_ID = "welcomeScreen"
    
    @IBOutlet var pageTitle: UINavigationItem!
    @IBOutlet weak var backgroundImage: UIImageView!
    @IBOutlet weak var loginButton: UIButton!
    
    //UI setup
    func checkFirstRun() -> Bool {
       let run = UserDefaults.standard.bool(forKey: .FIRST_RUN)
        return run
    }
    
    func setUI() {
        pageTitle.title = "מסך כניסה"
    }
    
    //Firebase interaction
    func signIn() {
        let loading = LoadingIndicatorController.openLoadingIndicator(controller: self)
        present(loading, animated: true) {
            TBFirAuthHandler.instance.loginAnonym(onComplete: {
                DispatchQueue.main.async {
                    loading.dismiss(animated: true, completion: {
                        self.SetDataFromDatabase()
                    })
                }
            }, onFail: { (e) in
                DispatchQueue.main.async {
                    loading.dismiss(animated: true, completion: {
                        let alert = AlertCreator.createErrorAlert(errorText: e.localizedDescription, onOkay: nil)
                        self.present(alert, animated: true, completion: nil)
                    })
                }
            })
        }
    }
    
    func SetDataFromDatabase() {
        let me = self
        let loading = LoadingIndicatorController.openLoadingIndicator(controller: self)
        present(loading, animated: true) {
            TBFirDatabaseHandler.instance.readDatabaseToMemory {
                DispatchQueue.main.async {
                    loading.dismiss(animated: true, completion: {
                        me.performSegue(withIdentifier: me.SEGUE_USER_TO_CATS, sender: me)
                    })
                }
            }
        }
    }

}

extension UINavigationController {
    
    override open var shouldAutorotate: Bool {
        guard let vc = visibleViewController
        else {
            return false
        }
        return vc.shouldAutorotate
    }
    
    override open var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        guard let vc = visibleViewController
            else {
                return .portrait
        }
        return vc.supportedInterfaceOrientations
    }
    
    override open var preferredInterfaceOrientationForPresentation: UIInterfaceOrientation {
        guard let vc = visibleViewController
            else {
                return .portrait
        }
        return vc.preferredInterfaceOrientationForPresentation
    }
}

//Overides
extension TBWelcomeViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        setUI()
        if (!checkFirstRun()) {
            let welcomeScreen = self.storyboard!.instantiateViewController(withIdentifier: WELCOME_SCREEN_ID)
            present(welcomeScreen, animated: true, completion: nil)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        switch Date.hourPeriod {
        case .morning:
            backgroundImage.image = #imageLiteral(resourceName: "Screen Background - MorningM")
//            loginButton.setTitleColor(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), for: .normal)
        case .noon:
            backgroundImage.image = #imageLiteral(resourceName: "Screen Background - NoonM")
//            loginButton.setTitleColor(#colorLiteral(red: 0.9019607843, green: 0.5333333333, blue: 0.2745098039, alpha: 1), for: .normal)
        default:
            backgroundImage.image = #imageLiteral(resourceName: "Screen Background - NightM")
//            loginButton.setTitleColor(#colorLiteral(red: 0.8392156863, green: 0.8392156863, blue: 0.8431372549, alpha: 1), for: .normal)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == SEGUE_USER_TO_CATS {
            let vc = segue.destination as! TBCategoryDisplayViewController
            vc.previousViewController = self
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        cache.removeAllObjects()
    }
    
    override open var shouldAutorotate: Bool {
        return true
    }
    
    override open var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return UIInterfaceOrientationMask.portrait
    }
    
    override open var preferredInterfaceOrientationForPresentation: UIInterfaceOrientation {
        return .portrait
    }
}

//IBActions
extension TBWelcomeViewController {
    @IBAction func userLoginDidTouch(_ sender: Any) {
        signIn()
    }
}


class LoadingIndicatorController {
    static func openLoadingIndicator(controller: UIViewController) -> UIViewController {
        return controller.storyboard!.instantiateViewController(withIdentifier: "loadingIndicator")
    }
}

class AlertCreator {
    
    static func createErrorAlert(errorText: String, onOkay: ((_ alertAction:UIAlertAction)->Void)? = nil) -> UIAlertController {
        let alert = UIAlertController(title: "אופס!", message: errorText, preferredStyle: .alert)
        let okayAction = UIAlertAction(title: "אוקיי", style: .default, handler: onOkay)
        alert.addAction(okayAction)
        return alert
    }
}
