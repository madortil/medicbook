//
//  PDFFileDisplayViewController.swift
//  TilBook
//
//  Created by Dan Fechtmann on 25/03/2018.
//  Copyright © 2018 Dan Fechtmann. All rights reserved.
//

import UIKit
import Firebase
import PDFKit

class TBPdfDisplayViewController: UIViewController, UISearchBarDelegate,UIDocumentPickerDelegate,UINavigationControllerDelegate{
    
    @IBOutlet weak var pdfRect: UIView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var pdfTitle: UINavigationItem!
    
    var pdfDocument = PDFDocument()
    var pdfView : PDFView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        searchBar.delegate = self
        do {
            let data = try fetchDataFromCache()
            print("Data fetch successful")
            displayPDF(pdfData: data, loading: self, isLoading: false)
        }
        catch {
            print("Data fetch unsuccessful")
            downloadPDF()
        }
        pdfTitle.title = currentPage.name
        self.navigationItem.rightBarButtonItems![0].isEnabled = isAdmin
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        pdfView?.frame = pdfRect.frame
    }
    
    //TODO implement calling downloading function, when it finishes, show the pdf
    func downloadPDF() {
        let loading = LoadingIndicatorController.openLoadingIndicator(controller: self)
        let pdfURL = currentPage.url
        present(loading, animated: true) {
            Storage.storage().reference().child(pdfURL).getData(maxSize: Int64(MAX_DOWNLOAD_SIZE)) { (data, error) in
                if let err = error {
                    DispatchQueue.main.async {
                        loading.dismiss(animated: true, completion: {
                            let alert = AlertCreator.createErrorAlert(errorText: err.localizedDescription)
                            self.present(alert, animated: true, completion: nil)
                        })
                    }
                    print(err.localizedDescription)

                }
                else {
                    let dataConverted = data! as NSData
                    let urlConverted = pdfURL as NSString
                    DispatchQueue.main.async {
                        self.displayPDF(pdfData: data!, loading: loading, isLoading: true)
                    }
                    cache.setObject(dataConverted, forKey: urlConverted)
                }
            }
        }
    }
    
    func fetchDataFromCache() throws -> Data {
        let url = currentPage.url as NSString
        guard let pdfData = cache.object(forKey: url)
            else {
                throw CacheErrors.dataDoesNotExistInCache
        }
        return pdfData as Data
    }
    
    func displayPDF (pdfData: Data, loading: UIViewController, isLoading: Bool) {
        if isLoading {
            loading.dismiss(animated: true) {
                self.pdfView = PDFView(frame: self.pdfRect.frame)
                let pdfDocument = PDFDocument(data: pdfData)
                self.pdfView!.document = pdfDocument
                self.pdfRect.addSubview(self.pdfView!)
            }
        }
        else {
            self.pdfView = PDFView(frame: self.pdfRect.frame)
            let pdfDocument = PDFDocument(data: pdfData)
            self.pdfView!.document = pdfDocument
            self.pdfRect.addSubview(self.pdfView!)
        }
    }
    
    @IBAction func uploadButtonTouched(_ sender: Any) {
        let documentMenu = UIDocumentPickerViewController(documentTypes: ["com.adobe.pdf"], in: .import)
        documentMenu.delegate = self
        documentMenu.modalPresentationStyle = .formSheet
        documentMenu.allowsMultipleSelection = false
        present(documentMenu, animated: true, completion: nil)
    }
    
    //TODO implement search bar functionality and highlighting
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        var pdfSelection : [PDFSelection] = []
        let searchText = searchBar.text!
        if(searchText != "") {
            DispatchQueue.global(qos: .userInteractive).async {
                pdfSelection = self.pdfDocument.findString(searchText, withOptions: .caseInsensitive)
                DispatchQueue.main.async {
                    self.showSearchResults(selection: pdfSelection)
                }
            }
        }
    }
    
    func showSearchResults(selection:[PDFSelection] = []) {
        
        for select in selection {
            select.color = UIColor.blue
            pdfView?.setCurrentSelection(select, animate: true)
        }
        pdfView?.scrollSelectionToVisible(self)
        
    }
    
    override open var shouldAutorotate: Bool {
        return true
    }
    
    override open var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .allButUpsideDown
    }
    
    override open var preferredInterfaceOrientationForPresentation: UIInterfaceOrientation {
        return .unknown
    }
    
    
    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
        print(urls[0].path)
        let me = self
        let alert = UIAlertController(title: "העלאת קובץ", message: "האם אתה בטוח שברצונך לעלות קובץ זה?", preferredStyle: .alert)
        let actionAccept = UIAlertAction(title: "כן", style: .destructive) { (action) in
            //TODO implement upload
            let loading = LoadingIndicatorController.openLoadingIndicator(controller: self)
            self.present(loading, animated: true, completion: {
                var data : Data = Data()
                do {
                    
                    data = try Data(contentsOf: URL(fileURLWithPath: urls[0].path))
                }
                catch {
                    
                }
                TBFirStorageHandler.instance.upload(pdfURL: currentPage.url, data: data, oncComplete: {
                    DispatchQueue.main.async {
                        loading.dismiss(animated: true, completion: {
                            let url = currentPage.url as NSString
                            cache.removeObject(forKey: url)
                            me.downloadPDF()
                        })
                    }
                }, onFail: { (e) in
                    DispatchQueue.main.async {
                        let alert = AlertCreator.createErrorAlert(errorText: e.localizedDescription)
                        me.present(alert, animated: true, completion: nil)
                    }
                })
            })
        }
        let actionCancel = UIAlertAction(title: "בטל", style: .default, handler: nil)
        alert.addAction(actionAccept)
        alert.addAction(actionCancel)
        present(alert, animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        cache.removeAllObjects()
    }
}

enum CacheErrors: Error {
    case dataDoesNotExistInCache
}
