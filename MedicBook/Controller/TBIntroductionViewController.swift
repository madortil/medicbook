//
//  welcomeViewController.swift
//  TilBook
//
//  Created by Dan Fechtmann on 30/08/2018.
//  Copyright © 2018 Dan Fechtmann. All rights reserved.
//

import UIKit

class TBIntroductionViewController: UIViewController {

    @IBOutlet weak var backgroundImage: UIImageView!
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        switch Date.hourPeriod {
        case .morning:
            backgroundImage.image = #imageLiteral(resourceName: "Screen Background - MorningL")
        case .noon:
            backgroundImage.image = #imageLiteral(resourceName: "Screen Background - NoonL")
        default:
            backgroundImage.image = #imageLiteral(resourceName: "Screen Background - NightL")
        }
    }
    
    @IBAction func continueDidTouch(_ sender: Any) {
        UserDefaults.standard.set(true, forKey: .FIRST_RUN)
        dismiss(animated: true, completion: nil)
    }
}
