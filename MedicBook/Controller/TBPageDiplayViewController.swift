//
//  PageDiplayViewController.swift
//  TilBook
//
//  Created by Dan Fechtmann on 25/03/2018.
//  Copyright © 2018 Dan Fechtmann. All rights reserved.
//

import UIKit
import Firebase

class TBPageDiplayViewController: UIViewController {
    
    @IBOutlet weak var categoryTitle: UILabel!
    @IBOutlet var addPageButton: UIBarButtonItem!
    @IBOutlet var pageTableView: UITableView!
    @IBOutlet weak var categoryImage: UIImageView!
    @IBOutlet weak var categoryThemeImage: UIImageView!
    
    let refreshControl = UIRefreshControl()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        TBFirDatabaseHandler.instance.observePageChange()
        
        switch Date.hourPeriod {
        case .morning:
            categoryImage.image = #imageLiteral(resourceName: "Category Theme - Morning")
            categoryTitle.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        case .noon:
            categoryImage.image = #imageLiteral(resourceName: "Category Theme - Noon")
            categoryTitle.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        default:
            categoryImage.image = #imageLiteral(resourceName: "Category Theme - Night")
            categoryTitle.textColor = #colorLiteral(red: 0.8392156863, green: 0.8392156863, blue: 0.8431372549, alpha: 1)
        }
        let numOfTheme = categories.firstIndex { $0.categoryKey == currentCategory.categoryKey }
        categoryThemeImage.image = UIImage(named: "Category Theme Glow \((numOfTheme ?? 1) % 10 + 1)")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        cache.removeAllObjects()
    }
    
    override open var shouldAutorotate: Bool {
        return true
    }
    
    override open var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    override open var preferredInterfaceOrientationForPresentation: UIInterfaceOrientation {
        return .portrait
    }
    
    func createEditAlert(currentCategoryIndex : Int) -> UIAlertController {
        let currentPage = currentCategory.pages[currentCategoryIndex]
        let alert = UIAlertController(title: "חלון עריכה", message: currentPage.name, preferredStyle: .alert)
        alert.addTextField { (textField) in
            textField.placeholder = "שם חדש"
        }
        let alertAccept = UIAlertAction(title: "שנה", style: .destructive) { (action) in
            //TODO finish implementation to change name in the database
            if let newName = alert.textFields![0].text {
                TBFirDatabaseHandler.instance.setNewPageName(categoryKey: currentCategory.categoryKey, pageKey: currentPage.pageKey, newName: newName, onComplete: {
                    DispatchQueue.main.async {
                        self.pageTableView.reloadData()
                    }
                })
            }
        }
        let alertCancel = UIAlertAction(title: "בטל", style: .cancel, handler: nil)
        
        alert.addAction(alertAccept)
        alert.addAction(alertCancel)
        
        return alert
    }
    
    func createDeleteAler(pageIndex: Int) -> UIAlertController {
        let me = self
        let alert = UIAlertController(title: "מחיקה", message: "אתה בטוח שברצונך למחוק את הקובץ?", preferredStyle: .alert)
        let acceptAction = UIAlertAction(title: "מחק", style: .destructive) { (action) in
            let loading = LoadingIndicatorController.openLoadingIndicator(controller: self)
            me.present(loading, animated: true, completion: {
                let page = currentCategory.pages[pageIndex]
                TBFirStorageHandler.instance.deletePDFFileFromDatabase(categoryKey: currentCategory.categoryKey, pageKey: page.pageKey, pdfURL: page.url, onComplete: {
                    DispatchQueue.main.async {
                        loading.dismiss(animated: true, completion: {
                            me.pageTableView.reloadData()
                        })
                    }
                }, onFail: { (e) in
                    DispatchQueue.main.async {
                        let alert = AlertCreator.createErrorAlert(errorText: e.localizedDescription)
                        me.present(alert, animated: true, completion: nil)
                    }
                })
            })
        }
        let cancelAction = UIAlertAction(title: "בטל", style: .default, handler: nil)
        alert.addAction(acceptAction)
        alert.addAction(cancelAction)
        return alert
    }
    
    func removeAllObservers() {
        Database.database().reference().removeAllObservers()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        removeAllObservers()
    }
    @IBAction func onNewPageTouched(_ sender: Any) {
        let me = self
        let loading = LoadingIndicatorController.openLoadingIndicator(controller: self)
        present(loading, animated: true) {
            TBFirDatabaseHandler.instance.setNewPageInDatabase(categoryKey: currentCategory.categoryKey, onPageAdded: {
                DispatchQueue.main.async {
                    loading.dismiss(animated: true, completion: {
                        me.pageTableView.reloadData()
                    })
                }
            }, onPageFailedToAdd: { (e) in
                DispatchQueue.main.async {
                    loading.dismiss(animated: true, completion: {
                        let alert = AlertCreator.createErrorAlert(errorText: e.localizedDescription)
                        me.present(alert, animated: true, completion: nil)
                    })
                }
            })
        }
    }
}

extension TBPageDiplayViewController:  UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return currentCategory.pages.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = pageTableView.dequeueReusableCell(withIdentifier: "pageCell") as! TBPageDisplayTableViewCell
        cell.pageLabel.text = currentCategory.pages[indexPath.row].name
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        currentPage = currentCategory.pages[indexPath.row]
        tableView.deselectRow(at: indexPath, animated: true)
        performSegue(withIdentifier: "showPDFFileDisplayViewController", sender: self)
    }
    
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return isAdmin
    }
    
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        let editAction = UITableViewRowAction(style: .normal, title: String.EDIT_TITLE) { (action, indexPath) in
            let alert = self.createEditAlert(currentCategoryIndex: indexPath.row)
            self.present(alert, animated: true, completion: nil)
        }
        let deleteAction = UITableViewRowAction(style: .destructive, title: String.DELETE_TITLE) { (action, indexPath) in
            //TODO add check if the pages are null, then delete
            let alert = self.createDeleteAler(pageIndex: indexPath.row)
            self.present(alert, animated: true, completion: nil)
        }
        return [editAction, deleteAction]
        
    }
    
}


extension TBPageDiplayViewController {
    
    private func setup() {
        self.pageTableView.refreshControl = refreshControl
        pageTableView.delegate = self
        pageTableView.dataSource = self
        
        addPageButton.isEnabled = isAdmin
        categoryTitle.text = currentCategory.name
    }
}
