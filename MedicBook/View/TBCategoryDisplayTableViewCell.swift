//
//  CategoryDisplayTableViewCell.swift
//  TilBook
//
//  Created by Dan Fechtmann on 19/03/2018.
//  Copyright © 2018 Dan Fechtmann. All rights reserved.
//

import UIKit

class TBCategoryDisplayTableViewCell: UITableViewCell {

    @IBOutlet var categoryNameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
