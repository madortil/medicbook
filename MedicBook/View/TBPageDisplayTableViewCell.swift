//
//  PageDisplayTableViewCell.swift
//  TilBook
//
//  Created by Dan Fechtmann on 25/03/2018.
//  Copyright © 2018 Dan Fechtmann. All rights reserved.
//

import UIKit

class TBPageDisplayTableViewCell: UITableViewCell {

    @IBOutlet var pageLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
